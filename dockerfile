FROM python:3.9

WORKDIR /opt/less1.1-gjango

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

RUN apt-get update
RUN apt-get install -y \
    gcc python3-dev musl-dev postgresql-client libpq-dev
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN pip install --upgrade pip

COPY ./requirements.txt .
COPY ./enrtypoint.sh .


RUN pip install -r requirements.txt

COPY . .
RUN chmod +x enrtypoint.sh
ENTRYPOINT [ "./enrtypoint.sh" ]
